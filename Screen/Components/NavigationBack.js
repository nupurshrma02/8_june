// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React from 'react';
import {View, Image, TouchableOpacity} from 'react-native';

const NavigationBack = (props) => {
  const Goback = () => {
  this.props.navigation.navigate('SettingsScreen')
  };
3
  return (
    <View style={{flexDirection: 'row'}}>
      <TouchableOpacity onPress={Goback}>
        <Image
          source={{
            uri:
              'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTkYjAVswR1bVZZjsC4Eui6iD_GemSinUf7JA&usqp=CAU',
          }}
          style={{width: 25, height: 25, marginLeft: 5, color: '#fff'}}
        />
      </TouchableOpacity>
    </View>
  );
};
export default NavigationBack;
